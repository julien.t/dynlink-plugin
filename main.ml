(*****************************************************************************)
(*                                                                           *)
(* Open Source License                                                       *)
(* Copyright (c) 2019 Nomadic Labs, <contact@nomadic-labs.com>               *)
(*                                                                           *)
(* Permission is hereby granted, free of charge, to any person obtaining a   *)
(* copy of this software and associated documentation files (the "Software"),*)
(* to deal in the Software without restriction, including without limitation *)
(* the rights to use, copy, modify, merge, publish, distribute, sublicense,  *)
(* and/or sell copies of the Software, and to permit persons to whom the     *)
(* Software is furnished to do so, subject to the following conditions:      *)
(*                                                                           *)
(* The above copyright notice and this permission notice shall be included   *)
(* in all copies or substantial portions of the Software.                    *)
(*                                                                           *)
(* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR*)
(* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,  *)
(* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL   *)
(* THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER*)
(* LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING   *)
(* FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER       *)
(* DEALINGS IN THE SOFTWARE.                                                 *)
(*                                                                           *)
(*****************************************************************************)

(** Compile the given file into a plugin.
    Necessary lib should be in [includes]
*)
let compile ?(includes=["./"])file =
  let output = Dynlink.adapt_filename file in
  let pp_includes =
    Format.pp_print_list
      (fun ppf s -> Format.fprintf ppf "-I %s" s) in
  let command =
    Format.asprintf "ocamlopt -shared %s %a -o  %s"
      file
      pp_includes includes
      output
  in
  if Sys.command command = 0
  then
    begin Format.eprintf "plugin compiled in %s\n command:\n  %s@."
        output command;
      output end
  else failwith ("compilation of "^file^" failed")

(** Build a plugin module out of a code.
    the code is injected inside a module with signature PLUGIN, and
    then the plugin registering function is called.
*)
let build_module plugin_file=
  let plugin_name, plugin_out = Filename.open_temp_file ~temp_dir:"./" "plugin_" ".ml"in
  let input_plugin = open_in plugin_file in
  Format.fprintf (Format.formatter_of_out_channel plugin_out) "
module Plugin : Loading.PLUGIN = struct
%a
end

let () = Loading.register (module Plugin)
@."
    (fun ppf inchan -> try
        while true do
          Format.fprintf ppf "%s@."
            (input_line inchan)
        done
      with End_of_file -> close_in inchan)
    input_plugin ;
  Format.eprintf "plugin written in %s@." plugin_name;
  plugin_name

(** Dynamically load the plugin *)
let load file =
  Format.eprintf "Loading %s@." file;
  Dynlink.loadfile (Dynlink.adapt_filename file)

let () =
  build_module "plugin.ml" |> compile |> load ;
  let module M = (val ! Loading.registered)
  in M.init () |> M.step |> M.finalize
