all:
	ocamlopt -c loading.ml
	ocamlfind opt -package dynlink -linkpkg loading.cmx main.ml -o	main.exe
clean :
	rm *.cm? *.exe *.o plugin_*
